#include <stdio.h>

int main()
{
   float h, r, vol;
   printf("Enter the height: ");
   scanf("%f", &h);
   printf("Enter the radius: ");
   scanf("%f", &r);
   vol = 3.14 * (r * r) * (h / 3);
   printf("Volume of the cone = %f", vol);
   return 0;
}