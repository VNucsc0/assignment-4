#include <stdio.h>

int main()
{
  int n1, n2;
  printf("Original value of first number: ");
  scanf("%d", &n1);
  printf("Original value of second number: ");
  scanf("%d", &n2);
  n1 ^= n2;
  n2 ^= n1;
  n1 ^= n2;
  printf("Number 1 after swapping = %d\n", n1);
  printf("Number 2 after swapping = %d", n2);
   return 0;
}